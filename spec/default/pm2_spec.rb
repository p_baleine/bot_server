require 'spec_helper'

describe command('sudo -iu vagrant pm2 --version') do
  it { should return_stdout '0.8.2' }
end
