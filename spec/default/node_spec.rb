require 'spec_helper'

describe command('sudo -iu vagrant node -v') do
  it { should return_stdout 'v0.10.28' }
end
